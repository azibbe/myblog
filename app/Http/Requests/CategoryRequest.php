<?php

namespace App\Http\Requests;

use function foo\func;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $category = $this->route()->parameter('category');

        $unique = 'unique:categories,name';
        if (!is_null($this->category)) {
            $unique .= ',' . $this->category->id;
        }

        return [
            'name'  =>  'required',$unique
        ];
    }
}
