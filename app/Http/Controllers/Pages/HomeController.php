<?php

namespace App\Http\Controllers\Pages;

use App\Post;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Pages\BaseController as Controller;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        $posts = Post::paginate(config('pagination.blog_page_limit'));

        return view('welcome')->with(compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function singlePost(Post $post)
    {
        return view('pages')->with(compact('post'));
    }

    public function search()
    {
        $keyword    =   request()->get('keyword');

        $posts  =   Post::where('title','LIKE','%'.$keyword.'%')
            ->orWhere('content','LIKE','%'.$keyword.'%')
            ->get();

        return view('search')->with(compact('posts'));
    }

    public function category(Category $category)
    {
//        dd($category->id);
        $posts  =   $category->posts()->paginate(5);

        return view('category')->with(compact('posts','category'));
    }
}
