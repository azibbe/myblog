<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class PostController extends Controller
{
    public function __construct()
    {
        $categories =   Category::pluck('name','id');

        View::share('categories', $categories);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts  =   Post::orderBy('created_at','DESC')->paginate(config('pagination.page_limit'));

//        $categories =   Category::pluck('name','id');

        return view('posts.index')->with(compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $route  =   route('post:store');
        return view('posts.create')->with(compact('route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {

//        auth()->user()->posts()->create($request->except('_token'));

        $post   =   auth()->user()->posts()->create($request->except('_token'));
        $post->categories()->sync($request->input('categories'));

        if($request->hasFile('image')){
            $image = $request->image->store('images');

            $post->update(['cover_image' => $image]);
        }

        return redirect()->route('post:index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function filter()
    {
        $keyword = request()->get('keyword');
        $category = request()->get('category');

//        $posts = new Post();
//        $posts = $posts->newQuery();
//
//        if ($category > 0) {
//            $posts->whereHas('categories', function ($query) use ($category) {
//                $query->where('id', $category);
//            });
//        }

        $posts = Post::when($category, function ($query) use ($category){
            $query->whereHas('categories', function ($query2) use ($category){
                $query2->where('id',$category);
            });
        })->when($keyword, function($query3) use ($keyword) {
            $query3->where('title','LIKE','%'.$keyword.'%');
//                ->orWhere('content','LIKE','%'.$keyword.'%')->paginate(5);
        })->paginate(5);

//        $posts->where('title','LIKE','%'.$keyword.'%')
//            ->where('content','LIKE','%'.$keyword.'%');

//        $posts->paginate(config('pagination.page_limit'));

        return view('posts.index')->with(compact('posts'));
    }
//        Post::where('title','LIKE','=',$keyword'%')


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit')->with(compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Post $post, PostRequest $request)
    {
//        dd($request->all());

        $post->update(request()->except('_token'));
        $post->update(['user_id' => auth()->user()->id]);

        if($request->hasFile('image')){
            $image = $request->image->store('images');

            $post->update(['cover_image' => $image]);
        }

        $post->categories()->sync(request()->input('categories'));

        return redirect()->route('post:index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('post:index');
    }
}
