<?php

namespace App\Traits;

trait Sluggable
{
    public function generateUniqueSlug($attribute,$separator = '-')
    {
        $slug   =   str_slug($attribute,$separator);

        $query  =   $this->newQuery();
        $list   =   $query->where('slug',$slug)
            ->orWhere('slug','LIKE',$slug . '-%');

        if($list->count() > 0){
            $last_slug  =   $this->first()->getAttribute('slug');

            $p  =   explode($separator, $last_slug);

            $number = intval(end($p));

            $slug .= $separator . ($number + 1);
        }

//        $this->attribute['slug'] = $slug;
    }

}
