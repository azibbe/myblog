<?php
    namespace App\Traits;

    use App\Role;

    trait HasRole
    {
        public function assignRole($role)
        {
            if( is_string($role)) {
                $role = Role::whereName($role)->first();
            }

            $this->roles()->attach($role);
        }

        public function revokeRole($role)
        {
            $role   =   Role::whereName($role)->first();

            return $this->roles()->detach($role);
        }

        public function userRoles()
        {
            return $this->roles()->get()->pluck('name');
        }

        public function hasRole($role)
        {
            // check role
            return $this->userRoles()->get()->has($role);

        }

    }