<?php

namespace App\Traits;

trait PostScope
{
    public function scopeLatest($query)
    {
        return $query->orderBy('created_at','DESC');
    }
//    Traits akan di include dalam Model Post
}