<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Sluggable;

class Post extends Model
{
    use Sluggable;

    protected $fillable = ['title','content','user_id','cover_image','slug'];

    protected $dispatchesEvents = [
        'creating' => 'App\Events\PostCreating',
        'updating'  =>  'App\Events\PostUpdating',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
