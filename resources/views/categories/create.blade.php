@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>
                    Create
                </h1>

                <div class="card">
                    <div class="card-header">Categories</div>

                    <div class="card-body">
                        {{--form>.form-group>label#{Title}+input.form-control#title^.form-group>label#content{Content}+textarea.form-control#content#content^button[type=submit].btn.btn-sm.btn-primary{Submit}--}}

                        @include('categories.form',['route' => route('category:store')])
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop