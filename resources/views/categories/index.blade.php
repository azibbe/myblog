@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>
                    Manage Categories
                    <div class="float-right">
                        <a href="{{ route('category:create') }}" class="btn btn-sm btn-success">New Category</a>
                    </div>
                </h1>

                <div class="card">
                    <div class="card-header">Categories</div>

                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th width="60%">Name</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $p)
                                <tr>
                                    <td>{{ $p->id }}</td>
                                    <td>{{ $p->name }}</td>
                                    <td>{{ date('d M Y', strtotime($p->created_at)) }}</td>
                                    <td>
                                        <a href="{!! route('category:edit', $p->id) !!}" class="btn btn-sm btn-primary">Edit</a>
                                        <a href="{!! route('category:delete', $p->id) !!}" onclick="return confirm('Sure?')" class="btn btn-sm btn-danger">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
{{--                        {{ $p->render() }}--}}
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop