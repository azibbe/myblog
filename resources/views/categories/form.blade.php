<form action="{{ $route }}" method="POST">
    @csrf
    <div class="form-group"><label for="" id="title">Name</label>
        <input type="text"
               name="name" value="{{ isset($category) ? $category->name : '' }}"
               class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
               id="name">
        @if($errors->has('name'))
            <div class="invalid-feedback">
                {{ $errors->first('name') }}
            </div>
        @endif
    </div>

    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
</form>