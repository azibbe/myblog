@extends('layouts.main')

@section('content')

    <!-- Blog Post -->
    <h3>Result for keyword "{{ request()->get('keyword') }}"</h3>
    <p>Number of results : {{ $posts->count() }}</p>
    @forelse($posts as $p)
    <div class="card mb-4">

        <div class="card-body">
            <h2 class="card-title">{{ $p->title }}</h2>
            <p class="card-text">{{ str_limit($p->content,200) }}</p>
            <a href="{{ route('page:pages', $p->id) }}" class="btn btn-primary">Read More &rarr;</a>
        </div>
        <div class="card-footer text-muted">
            Posted on {{ $p->created_at->format('F d, Y') }} by
            <a href="#">{{ $p->user->name }}</a>
        </div>
    </div>
    @empty
        <p>No result found.</p>
    @endforelse


@endsection

@section('categories')
    @include('shared.categories_block',['categories'=>$categories])
@endsection
