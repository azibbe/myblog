@extends('layouts.main')

@section('content')
    @include('shared.post_block',['posts' => $posts])
@endsection

{{--@section('categories')--}}
    {{--@include('shared.categories_block',['posts' => $posts,'category' => $categories])--}}
{{--@endsection--}}