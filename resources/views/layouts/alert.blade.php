@if(session()->has('message'))
<div class="alert {{ session()->get('type', 'alert-success')}}">
    {{ session()->get('message') }}
</div>
@endif