@extends('layouts.main')

@section('content')

        @include('shared.post_block',['posts' => $posts])

@endsection

@section('categories')
    @include('shared.categories_block',['categories'=>$categories])
@endsection

@push('widget')
    @include('shared.widget',['title'=>'Widget 1','body'=>'@push'])
@endpush

@prepend('widget')
    @include('shared.widget',['title'=>'Widget 2','body'=>'@prepend'])
@endprepend
