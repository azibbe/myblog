<form action="{{ $route }}" method="POST">
    @csrf
    <div class="form-group"><label for="" id="title">Title</label>
        <input type="text" name="title"
               class="form-control {{ $errors->has('title') ? 'is-invalid':'' }}"
               id="title" value="{{ old('title') }}">

        @if($errors->has('title'))
            <div class="invalid-feedback">
                {{ $errors->first('title') }}
            </div>
        @endif
    </div>

    <div class="form-group"><label for="" id="content">Content</label>
        <textarea
                id="content"
                cols="30"
                rows="5"
                name="content"
                class="form-control {{ $errors->has('content') ? 'is-invalid':'' }}">{{ isset($post) ? $post->content :old('content') }}
        </textarea>
        @if($errors->has('content'))
            <div class="invalid-feedback">
                {{ $errors->first('content') }}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="" id="categories">Categories</label>
        <select name="categories[]" id="" class="form-control" multiple>
            @foreach($categories as $id => $value)
                @if($post->categories()->get()->contains($id))

                    <option value="{{ $id }}" selected>{{ $value }}</option>
                @else
                    <option value="{{ $id }}">{{ $value }}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="image" id="image">Cover Image</label>
        <input type="file" name="image" id="image" class="form-control">
    </div>

    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
</form>