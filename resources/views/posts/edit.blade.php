@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>
                    Create
                </h1>

                <div class="card">
                    <div class="card-header">Posts</div>

                    <div class="card-body">
                        {{--form>.form-group>label#{Title}+input.form-control#title^.form-group>label#content{Content}+textarea.form-control#content#content^button[type=submit].btn.btn-sm.btn-primary{Submit}--}}

                        <form action="{!! route('post:update',$post->id) !!}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group"><label for="" id="title"></label>
                                <input type="text" name="title" value="{{ $post->title }}"
                                                                                                class="form-control"
                                                                                                id="title">
                            </div>
                            <div class="form-group"><label for="" id="content">Content</label>
                                <textarea
                                                                                                         id="content content"
                                                                                                         cols="30"
                                                                                                         rows="5"
                                          name="content"
                                                                                                         class="form-control"> {{ $post->content }}
                                </textarea>
                            </div>

                            <div class="form-group">
                                <label for="" id="categories">Categories</label>
                                <select name="categories[]" id="" class="form-control" multiple>
                                    @foreach($categories as $id => $value)
                                        @if($post->categories()->get()->contains($id))

                                        <option value="{{ $id }}" selected>{{ $value }}</option>
                                        @else
                                            <option value="{{ $id }}">{{ $value }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="image" id="image">Cover Image</label>
                                <input type="file" name="image" id="image" class="form-control {{ $errors->has('image') ? 'is-invalid':'' }}">


                            @if($errors->has('image'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('image') }}
                                </div>
                            @endif
                            </div>

                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop