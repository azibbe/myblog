@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>
                    Manage Posts
                    <div class="float-right">
                        <a href="{{ route('post:create') }}" class="btn btn-sm btn-success">New Post</a>
                    </div>
                </h1>

                <div class="card">
                    <div class="card-header">Posts</div>

                    <div class="card-body">

                        <form action="{{ route('post:search') }}" method="get" class="form-inline">

                                <input type="text" name="keyword" value="{{ request()->get('keyword') }}" class="form-control mb-2 mr-sm-2" placeholder="Search..">
                            <select name="category" class="form-control mb-2 mr-sm-2">
                                <option value="0">All</option>
                                @foreach($categories as $id => $name)
                                    <option value="{{ $id }}" {{ request()->get('category') == $id ? 'selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>

                            <button type="submit" class="btn btn-primary mb-2">Search</button>
                        </form>

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=0; ?>
                            @foreach($posts as $p)
                                <tr>
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $p->title }}</td>
                                    <td>{{ $p->user->name }}</td>
                                    <td>{{ date('d M Y', strtotime($p->created_at)) }}</td>
                                    <td>
                                        <a href="{{ route('post:edit', $p->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                        <a href="{{ route('post:delete', $p->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>

                        </table>
                        {{--{{ $posts->render() }}--}}
                        {{ $posts->appends(['keyword' => request()->get('keyword'),
                        'category'=> request()->get('category')])->links() }}
                    </div>

                </div>

            </div>
        </div>
    </div>
@stop