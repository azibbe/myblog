@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>
                    Create
                </h1>

                <div class="card">
                    <div class="card-header">Posts</div>

                    <div class="card-body">
                        {{--form>.form-group>label#{Title}+input.form-control#title^.form-group>label#content{Content}+textarea.form-control#content#content^button[type=submit].btn.btn-sm.btn-primary{Submit}--}}

                        <form action="{{ $route }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group"><label for="" id="title">Title</label>
                                <input type="text" name="title"
                                       class="form-control {{ $errors->has('title') ? 'is-invalid':'' }}"
                                       id="title" value="{{ old('title') }}">

                                @if($errors->has('title'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group"><label for="" id="content">Content</label>
                                <textarea
                                        id="content"
                                        cols="30"
                                        rows="5"
                                        name="content"
                                        class="form-control {{ $errors->has('content') ? 'is-invalid':'' }}">{{ isset($post) ? $post->content :old('content') }}
        </textarea>
                                @if($errors->has('content'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('content') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="" id="categories">Categories</label>
                                <select name="categories[]" id="" class="form-control" multiple>
                                    @foreach($categories as $id => $value)
                                            <option value="{{ $id }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="image" id="image">Cover Image</label>
                                <input type="file" name="image" id="image" class="form-control {{ $errors->has('image') ? 'is-invalid':'' }}">
                            </div>
                            @if($errors->has('image'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('image') }}
                                </div>
                            @endif

                            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop