<div class="card my-4">
    <h5 class="card-header">{{ $title }}</h5>
    <div class="card-body">
        {{ $body }}
    </div>
</div>