@foreach($posts as $p)
    <div class="card mb-4">
    @if(!is_null($p->cover_image))
        <img class="card-img-top" src="{{ url('/'.$p->cover_image) }}" alt="Card image cap">
    @endif

        <div class="card-body">
            <h2 class="card-title">{{ $p->title }}</h2>
            <p class="card-text">{{ str_limit($p->content,200) }}</p>
            <a href="{{ route('page:pages', $p->id) }}" class="btn btn-primary">Read More &rarr;</a>
        </div>
        <div class="card-footer text-muted">
            Posted on {{ $p->created_at->format('F d, Y') }} by
            <a href="#">{{ $p->user->name }}</a>
        </div>
    </div>
@endforeach

<ul class="pagination justify-content-center mb-4">
    <li class="page-item {{ $posts->currentPage() === 1 ? 'disabled' : '' }}">
        <a class="page-link" href="{{ $posts->previousPageUrl() }}">&larr; Older</a>
    </li>
    <li class="page-item {{ $posts->hasMorePages() ? '' : 'disabled' }}">
        <a class="page-link" href="{{ $posts->nextPageUrl() }}">Newer &rarr;</a>
    </li>
</ul>