<?php

use Faker\Generator as Faker;

$factory->define('App\Category', function (Faker $faker) {
    return [
        'name' => $faker->words(3,true)
    ];
});
