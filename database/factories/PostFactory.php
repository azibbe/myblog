<?php

use Faker\Generator as Faker;
use App\Post;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title'     => $faker->text(50),
        'content'   => $faker->text(250),
        'user_id'   => App\User::all()->random()->id,
        'slug'      =>  $faker->unique()->slug()
    ];
});
