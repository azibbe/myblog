<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Method 1
        //factory('App\Category', 5)->create();

        //Method 2
        $categories = ['Global','Invest','Stock','Market','Analysis'];

        foreach ($categories as $category)
        {
            // Method 2.1
//            $c = new App\Category();
//            $c->name = $category;
//            $c->save();

            // Method 2.2
            // Nota : Mesti guna fillable at category Model
            App\Category::create(['name' => $category]);
        }

    }
}
