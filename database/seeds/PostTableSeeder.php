<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Post',20)->create()
        ->each(function ($p){
//              Method 1
            $p->categories()
//                ->attach(App\Category::all()
//                ->random());

          //Method 2
            ->sync(App\Category::all()->random(1,2)->pluck('id'));
    });
    }
}
