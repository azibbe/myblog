<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles  =   ['Admin','Author','User'];

        foreach ($roles as $role) {
            $r  =   new \App\Role();
            $r->name = $role;
            $r->save();
        }
    }
}
