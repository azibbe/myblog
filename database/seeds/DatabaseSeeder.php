<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Note : empty kan table dulu.  DB::table('users')->truncate();

        DB::table('categories')->truncate();
        DB::table('posts')->truncate();
        DB::table('users')->truncate();
        DB::table('roles')->truncate();
        DB::table('role_user')->truncate();

         $this->call(RoleTableSeeder::class);
         $this->call(UserTableSeeder::class);
         $this->call(CategorySeeder::class);
         $this->call(PostTableSeeder::class);


        //Note : cara lain $this->call('PostTableSeeder');
    }
}
