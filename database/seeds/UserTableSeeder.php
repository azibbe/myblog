<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory('App\User',5)->create();

//        factory('App\User')->times(100)->create();

        // Note : Ade 2 method
        factory('App\User')->times(10)->create()
            ->each(function ($u) {
//                dd($u);
//                $u->roles()->attach(\App\Role::get());
                $u->assignRole(\App\Role::get()->random());
//                dd(\App\Role::get()->random());

        });
    }
}
