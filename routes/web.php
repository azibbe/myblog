<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'=>'Pages','as' => 'page:'], function (){
    Route::get('/', 'HomeController@welcome')->name('home');
    Route::get('/pages/{post}','HomeController@singlePost')->name('pages');
    Route::get('/search','HomeController@search')->name('search');
    Route::get('/categories/{category}', 'HomeController@category')->name('category');
});

Auth::routes();


Route::group(['prefix' => 'admin','middleware' => 'auth','can:manage,App\User'], function () {

    Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    Route::group(['prefix' => 'posts','as' => 'post:'], function()
    {

        Route::get('/', 'PostController@index')->name('index');
        Route::get('/create', 'PostController@create')->name('create');
        Route::post('/create', 'PostController@store')->name('store');
        Route::get('/edit/{post}', 'PostController@edit')->name('edit');
        Route::post('/update/{post}', 'PostController@update')->name('update');
        Route::get('/delete/{post}', 'PostController@destroy')->name('delete');
        Route::get('/search','PostController@filter')->name('search');
    });

    Route::group(['prefix' => 'categories','as' => 'category:'], function()
    {

        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('/create', 'CategoryController@create')->name('create');
        Route::post('/create', 'CategoryController@store')->name('store');
        Route::get('/edit/{category}', 'CategoryController@edit')->name('edit');
        Route::post('/update/{category}', 'CategoryController@update')->name('update');
        Route::get('/delete/{category}', 'CategoryController@destroy')->name('delete');
    });
});

